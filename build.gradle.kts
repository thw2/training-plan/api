val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val mockserver_netty_version: String by project
val skrapeit_version: String by project

plugins {
    application
    kotlin("jvm") version "1.8.10"
    id("io.ktor.plugin") version "2.3.1"
    kotlin("plugin.serialization") version "1.8.10"
}

group = "de.danisbu"
version = "0.0.1"
application {
    mainClass.set("de.danisbu.ApplicationKt")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    maven {
        url = uri("https://gitlab.com/api/v4/projects/40318647/packages/maven")
        name = "GitLab"
        credentials(HttpHeaderCredentials::class) {
            name = System.getenv("TOKEN_TYPE") ?: findProperty("tokenType") as String?
            value = System.getenv("CI_JOB_TOKEN") ?: findProperty("gitlabDeployToken") as String?
        }
        authentication {
            create("header", HttpHeaderAuthentication::class)
        }
    }
    mavenCentral()
}

dependencies {
    implementation("io.ktor:ktor-server-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-host-common-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-status-pages-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-netty-jvm:$ktor_version")
    implementation("io.ktor:ktor-serialization-kotlinx-json:$ktor_version")
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("it.skrape:skrapeit:$skrapeit_version")
    implementation("it.skrape:skrapeit-http-fetcher:$skrapeit_version")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0")
    implementation("de.danisbu.thw.trainingplan:domain:1.1-SNAPSHOT")
    implementation("io.ktor:ktor-serialization-jackson:$ktor_version")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.14.2")

    testImplementation("io.ktor:ktor-server-tests-jvm:$ktor_version")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
    testImplementation("org.mock-server:mockserver-netty:$mockserver_netty_version")
}

ktor {
    docker {
        jreVersion.set(JreVersion.JRE_18)
        localImageName.set("thw-training-plan-api")
        imageTag.set(version.toString())

        portMappings.set(listOf(
            io.ktor.plugin.features.DockerPortMapping(
                80,
                8080,
                io.ktor.plugin.features.DockerPortMappingProtocol.TCP
            )
        ))

        externalRegistry.set(
            io.ktor.plugin.features.DockerImageRegistry.dockerHub(
                appName = provider { "thw-training-plan-api" },
                username = provider { System.getenv("DOCKER_USER") ?: (findProperty("dockerUser") as String? ?: "") },
                password = provider { System.getenv("DOCKER_PASSWORD") ?:  (findProperty("dockerPassword") as String? ?: "") }
            )
        )
    }
}

jib.from.auth{
    username = System.getenv("DOCKER_USER") ?: (findProperty("dockerUser") as String? ?: "")
    password = System.getenv("DOCKER_PASSWORD") ?: (findProperty("dockerPassword") as String? ?: "")
}
jib.to.auth{
    username = System.getenv("DOCKER_USER") ?: (findProperty("dockerUser") as String? ?: "")
    password = System.getenv("DOCKER_PASSWORD") ?: (findProperty("dockerPassword") as String? ?: "")
}
