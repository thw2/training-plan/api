package de.danisbu.thw.trainingplan.plugins

import de.danisbu.thw.trainingplan.endpoints.Courses
import io.ktor.server.routing.*
import io.ktor.http.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.application.*
import io.ktor.server.response.*

fun Application.configureRouting() {
    install(StatusPages) {
        exception<AuthenticationException> { call, _ ->
            call.respond(HttpStatusCode.Unauthorized)
        }
        exception<AuthorizationException> { call, _ ->
            call.respond(HttpStatusCode.Forbidden)
        }
    }

    routing {
        get("/") {
            call.respondText("Hello World!")
        }
        get("/courses") {
            val trainingList = Courses().scrap()
            call.respond(trainingList)
        }
    }
}

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()
