package de.danisbu.thw.trainingplan.endpoints

import de.danisbu.thw.trainingplan.controller.Scrapper
import de.danisbu.thw.trainingplan.modell.TrainingList

class Courses {
    fun scrap(): TrainingList {
        return Scrapper().scrap()
    }
}