package de.danisbu.thw.trainingplan.controller

import de.danisbu.thw.trainingplan.builder.TrainingBuilder
import de.danisbu.thw.trainingplan.modell.*
import it.skrape.core.htmlDocument
import it.skrape.fetcher.HttpFetcher
import it.skrape.fetcher.extractIt
import it.skrape.fetcher.skrape
import it.skrape.selects.html5.dd
import kotlinx.datetime.toKotlinLocalDateTime
import org.slf4j.LoggerFactory
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class Scrapper (val requestUrl: String = "https://www.thw-ausbildungszentrum.de/THW-BuS/DE/Ausbildungsangebot/Lehrgangskalender/lehrgangskalender_node.html") {
    private val logger = LoggerFactory.getLogger(Scrapper::class.java)

    fun scrap(): TrainingList {
        logger.error("try")
        return skrape(HttpFetcher) {
            request {
                url = requestUrl
            }

            extractIt<TrainingList> { trainingList ->
                htmlDocument{
                    relaxed = true

                    ".teaserlist" {
                        ".teaser" {
                            findAll{
                                logger.error("test")
                                 this.map {
                                    val trainingBuilder = TrainingBuilder()
                                    it.findFirst(".docData") {
                                        dd {
                                            findFirst {
                                                trainingBuilder.start = Start(
                                                    LocalDateTime.parse(
                                                        this.text,
                                                        DateTimeFormatter.ofPattern("EE' 'dd.MM.yyyy, HH:mm 'Uhr'").withLocale(
                                                            Locale.GERMAN)
                                                    ).toKotlinLocalDateTime()
                                                )
                                            }
                                            findLast {
                                                trainingBuilder.end = End(
                                                    LocalDateTime.parse(
                                                        this.text,
                                                        DateTimeFormatter.ofPattern("EE' 'dd.MM.yyyy, kk:mm 'Uhr'").withLocale(
                                                            Locale.GERMAN)
                                                    ).toKotlinLocalDateTime()
                                                )
                                            }
                                        }
                                    }

                                    it.findFirst(".courseAction") {
                                        findFirst("dd") {
                                            if(this.text.length > 0){
                                                trainingBuilder.registrationDeadline = RegistrationDeadline(
                                                    LocalDate.parse(
                                                        this.text,
                                                        DateTimeFormatter.ofPattern("dd.MM.yyyy")
                                                    ).atStartOfDay().toKotlinLocalDateTime()
                                                )
                                            } else {
                                                trainingBuilder.registrationDeadline = null
                                            }
                                        }
                                    }

                                    it.findFirst("h2") {
                                        findFirst {
                                            val splittedAndTrimedByMinus = this.text.split("-").map{ it.trim() }
                                            val splittedFirstPart = splittedAndTrimedByMinus.first().split(" ")

                                            trainingBuilder.courseBuilder.title = Title(splittedAndTrimedByMinus.last())
                                            trainingBuilder.courseBuilder.type = Type(splittedFirstPart.first())
                                            trainingBuilder.courseBuilder.id= CourseId(splittedFirstPart.last())
                                        }
                                    }

                                    it.findFirst(".metadata"){
                                        val splittedAndTrimed = this.text.removePrefix("Standort").split("–").map { it.trim() }

                                        trainingBuilder.location = Location(splittedAndTrimed.first())
                                        trainingBuilder.id = TrainingId(splittedAndTrimed.last())
                                    }

                                    trainingList.list.add(trainingBuilder.built())
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}