package de.danisbu.controller

import de.danisbu.HttpTestBase
import de.danisbu.thw.trainingplan.controller.Scrapper
import de.danisbu.thw.trainingplan.modell.*
import kotlinx.datetime.toKotlinLocalDateTime
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import java.time.LocalDateTime

class ScrapperTest : HttpTestBase() {
    val path = "/THW-BuS/DE/Ausbildungsangebot/Lehrgangskalender/lehrgangskalender_node.html"
    val expected = TrainingList(
        mutableListOf(
            Training(
                TrainingId("H 234/22"),
                Start(LocalDateTime.of(2022, 8, 22, 8, 0, 0, 0).toKotlinLocalDateTime()),
                End(LocalDateTime.of(2022, 8, 24, 12, 0, 0, 0).toKotlinLocalDateTime()),
                RegistrationDeadline(LocalDateTime.of(2022, 6, 29, 0, 0, 0, 0).toKotlinLocalDateTime()),
                Course(
                    Title("Systemadministration im OV"),
                    Type("Ausb"),
                    CourseId("11")
                ),
                Location("Hoya")
            ),
            Training(
                TrainingId("NI 12/22"),
                Start(LocalDateTime.of(2022, 8, 22, 8, 0, 0, 0).toKotlinLocalDateTime()),
                End(LocalDateTime.of(2022, 8, 26, 9, 30, 0, 0).toKotlinLocalDateTime()),
                RegistrationDeadline(LocalDateTime.of(2022, 6, 27, 0, 0, 0, 0).toKotlinLocalDateTime()),
                Course(
                    Title("Thermisches Trennen im THW"),
                    Type("Spez"),
                    CourseId("10")
                ),
                Location("Nienburg (Weser)")
            ),
            Training(
                TrainingId("N 116/22"),
                Start(LocalDateTime.of(2022, 8, 22, 8, 0, 0, 0).toKotlinLocalDateTime()),
                End(LocalDateTime.of(2022, 8, 26, 11, 0, 0, 0).toKotlinLocalDateTime()),
                RegistrationDeadline(LocalDateTime.of(2022, 6, 27, 0, 0, 0, 0).toKotlinLocalDateTime()),
                Course(
                    Title("Grundlagen Führung"),
                    Type("FüUF"),
                    CourseId("26")
                ),
                Location("Neuhausen a.d.F.")
            ),
            Training(
                TrainingId("H 259/22"),
                Start(LocalDateTime.of(2022, 9, 19, 8, 0, 0, 0).toKotlinLocalDateTime()),
                End(LocalDateTime.of(2022, 9, 23, 10, 30, 0, 0).toKotlinLocalDateTime()),
                null,
                Course(
                    Title("Grundlagen Führung"),
                    Type("FüUF"),
                    CourseId("26")
                ),
                Location("Hoya")
            )
        )
    )

    @Test
    fun scrap() {
        val content = this::class.java.getResource("/THW-AZ Lehrgangskalender.htm")?.readText()

        mockServer.`when`(
            request()
                .withMethod("GET")
                .withPath(path)
        ).respond(
            response()
                .withStatusCode(200)
                .withBody(content)
        )

        val scrapedData = Scrapper("$uri$path").scrap()

        assertThat(scrapedData, `is`(expected))
    }
}